package com.ray;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParkmanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParkmanagementApplication.class, args);
	}

}
